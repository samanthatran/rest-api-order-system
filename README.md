Start server with the command `./gradlew bootRun`

REST API endpoints:

Changes added:
HTTP Method: GET
Endpoint: /customers/customerArray
Body (JSON format): {"firstName": "*firstName*", "lastName": "*lastName*"}
Return (JSON format): Returns Array of customers

HTTP Method: PUT
Endpoint: /customers/*number*
Body (JSON format): {"firstName": "*firstName*", "lastName": "*lastName*"}
Return (JSON format): Changes firstName and lastName from cust no. input

Method | Endpoint | Body | Return | Note
--- | --- | --- | --- | ---
POST | /customers/new | {"firstName": "*firstName*", "lastName": "*lastName*" } | *customerNumber* | Adds new customer to database
GET | /customers/*number* | | {"number" : *number*, "firstName": "*firstName*", "lastName": "*lastName*"} | Returns customer with given number
POST | /products/new | {"price": *price*, "description": *description*} | *sku* | Adds new product to database
GET | /products/*sku* | | {"sku": *sku*, "price": *price*, "description": *description*} | Returns product with given number
POST | /orders/new/*customerNumber* | | *orderNumber* | Adds new order to database for this customer
GET | /orders/*number*/customer | | *customerNumber* | Returns customer for this order number
GET | /orders/*number*/lines | | [{"sku": *sku*, "quantity": *quantity*}, ...] | Returns array of order lines for this order
PUT | /orders/*number*/lines | {"sku": *sku*, "quantity": *quantity*} | | Increase quantity of the product in this order
DELETE | /orders/*number*/lines | {"sku": *sku*, "quantity": *quantity*} | | Decrease quantity of the product in this order
GET | /customers/customerArray | | {"firstName": "*firstName*", "lastName": "*lastName*"} | Returns Array of customers
PUT | /customers/*number* | {"firstName": "*firstName*", "lastName": "*lastName*"} | Changes firstName and lastName by cust. no input